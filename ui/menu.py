# -*- coding: utf-8 -*-

## \package ui.menu

# MIT licensing
# See: docs/LICENSE.txt


import wx

from dbr.config			import ConfCode
from dbr.config			import GetDefaultConfigValue
from dbr.config			import ReadConfig
from dbr.language		import GT
from dbr.log			import DebugEnabled
from globals.bitmaps	import ICON_CLOCK
from globals.bitmaps	import ICON_GLOBE
from globals.bitmaps	import ICON_LOGO
from globals.execute	import GetExecutable
from globals.ident		import menuid, refid
from startup.tests		import GetTestList


## A menu bar that stores an ID along with a menu
class MenuBar(wx.MenuBar):
	## Constructor
	#
	#  \param parent
	#	<b><i>wx.Window</i></b> parent window of the menu bar
	#	If not None, automatically sets itself as parent's menu bar
	#  \param style
	#	Menu bar style represented by an <b><i>integer</i></b> value
	def __init__(self, parent=None, style=0):
		wx.MenuBar.__init__(self, style)

		self.id_list = []

		if isinstance(parent, wx.Frame):
			parent.SetMenuBar(self)


	## Append a menu to the end of menu bar
	#
	#  \param menu
	#	<b><i>wx.Menu</i></b> instance to be appended
	#  \param title
	#	Label to be displayed in the menu bar
	#  \param menuId
	#	Unique <b><i>integer</i></b> identifier to store for menu
	def Append(self, menu, title, menuId):
		self.id_list.append(menuId)

		return wx.MenuBar.Append(self, menu, title)


	## Finds a wx.Menu by ID
	#
	#  \param menuId
	#	Menu <b><i>integer</i></b> identifier to search for in menu bar
	#  \return
	#	The <b><i>wx.Menu</i></b> with using identifier
	def GetMenuById(self, menuId):
		m_index = self.id_list.index(menuId)

		return self.GetMenu(m_index)


	## Insert a menu to a specified position in the menu bar
	#
	#  \param index
	#	Position index to insert menu
	#  \param menu
	#	<b><i>wx.Menu</i></b> instance to be inserted
	#  \param title
	#	Label to be displayed in the menu bar
	#  \param menuId
	#	Unique <b><i>integer</i></b> identifier to store for menu
	def Insert(self, index, menu, title, menuId):
		self.id_list.insert(index, menuId)

		return wx.MenuBar.Insert(self, index, menu, title)


## Creates the main menu bar.
#
# \param parent Main wx.Frame window
# \return New MenuBar instance
def createMenuBar(parent):
	testing = u'alpha' in GetTestList() or DebugEnabled()

	menubar = MenuBar(parent)

	menu_file = wx.Menu()

	menubar.Append(menu_file, GT(u'文件'), menuid.FILE)
	# This menu is filled from wiz.wizard.Wizard.SetPages
	menubar.Append(wx.Menu(), GT(u'页面'), menuid.PAGE)

	# *** File Menu *** #

	mitems_file = [
		(menuid.NEW, GT(u'新建工程'), GT(u'开始一个新的工程'),),
		(menuid.OPEN, GT(u'打开'), GT(u'打开一个以前保存的工程'),),
		(menuid.SAVE, GT(u'保存'), GT(u'保存当前工程'),),
		(menuid.SAVEAS, GT(u'另存为'), GT(u'保存当前工程到一个新的文件'),),
		None,
		(menuid.QBUILD, GT(u'快速构建'), GT(u'从已存在的构建树中构建一个软件包'), ICON_CLOCK,),
		None,
		(menuid.EXIT, GT(u'退出'), GT(u'退出Debreate'),),
		]

	if testing:
		mitems_file.append((menuid.ALIEN, GT(u'Convert packages'), GT(u'Convert between package types')))

	# Adding all menus to menu bar

	mitems = (
		mitems_file,
		)

	for menu_list in mitems:
		for mitem in menu_list:
			if not mitem:
				menu_file.AppendSeparator()

			else:
				itm = wx.MenuItem(menu_file, mitem[0], mitem[1], mitem[2])
				if len(mitem) > 3:
					itm.SetBitmap(mitem[3])

				menu_file.AppendItem(itm)

	# ----- Options Menu
	parent.menu_opt = wx.Menu()

	# Show/Hide tooltips
	parent.opt_tooltips = wx.MenuItem(parent.menu_opt, menuid.TOOLTIPS, GT(u'显示提示信息'),
			GT(u'显示或者隐藏提示信息'), kind=wx.ITEM_CHECK)

	# A bug with wx 2.8 does not allow tooltips to be toggled off
	if wx.MAJOR_VERSION > 2:
		parent.menu_opt.AppendItem(parent.opt_tooltips)

	if parent.menu_opt.FindItemById(menuid.TOOLTIPS):
		show_tooltips = ReadConfig(u'tooltips')
		if show_tooltips != ConfCode.KEY_NO_EXIST:
			parent.opt_tooltips.Check(show_tooltips)

		else:
			parent.opt_tooltips.Check(GetDefaultConfigValue(u'tooltips'))

		parent.OnToggleToolTips()

	# *** Option Menu: open logs directory *** #

	if GetExecutable(u'xdg-open'):
		mitm_logs_open = wx.MenuItem(parent.menu_opt, menuid.OPENLOGS, GT(u'打开日志目录'))
		parent.menu_opt.AppendItem(mitm_logs_open)

		wx.EVT_MENU(parent, menuid.OPENLOGS, parent.OnLogDirOpen)

	# *** OS distribution names cache *** #

	opt_distname_cache = wx.MenuItem(parent.menu_opt, menuid.DIST, GT(u'更新发行版命名缓存'),
			GT(u'创建/更新发新版本命名的改变日志页面的列表'))
	parent.menu_opt.AppendItem(opt_distname_cache)

	mitm_ccache = wx.MenuItem(parent.menu_opt, menuid.CCACHE, GT(u'清除本地缓存'))
	parent.menu_opt.AppendItem(mitm_ccache)

	# ----- Help Menu
	menu_help = wx.Menu()

	# ----- Version update
	mitm_update = wx.MenuItem(menu_help, menuid.UPDATE, GT(u'检查更新'),
			GT(u'检查是否有新版本可以下载'))
	mitm_update.SetBitmap(ICON_LOGO)

	menu_help.AppendItem(mitm_update)
	menu_help.AppendSeparator()

	# Menu with links to the Debian Policy Manual webpages
	parent.menu_policy = wx.Menu()

	policy_links = (
		(refid.DPM, GT(u'Debian参考手册'),
				u'https://www.debian.org/doc/debian-policy',),
		(refid.DPMCtrl, GT(u'Control文件'),
				u'https://www.debian.org/doc/debian-policy/ch-controlfields.html',),
		(refid.DPMLog, GT(u'改变日志'),
				u'https://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog',),
		(refid.UPM, GT(u'Ubuntu参考手册'),
				u'http://people.canonical.com/~cjwatson/ubuntu-policy/policy.html/',),
		(refid.LINT_TAGS, GT(u'Lintian具体描述'),
				u'https://lintian.debian.org/tags-all.html',),
		(refid.LINT_OVERRIDE, GT(u'覆盖Lintian标记'),
				u'https://lintian.debian.org/manual/section-2.4.html',),
		(refid.LAUNCHERS, GT(u'启动器 / 桌面快捷方式'),
				u'https://www.freedesktop.org/wiki/Specifications/desktop-entry-spec/',),
		# Unofficial links
		None,
		(refid.DEBSRC, GT(u'从源构建deb软件包'),
				u'http://www.quietearth.us/articles/2006/08/16/Building-deb-package-from-source',), # This is here only temporarily for reference
		(refid.MAN, GT(u'书写说明页面'),
				u'https://liw.fi/manpages/',),
		)

	for LINK in policy_links:
		if not LINK:
			parent.menu_policy.AppendSeparator()

		elif len(LINK) > 2:
			link_id = LINK[0]
			label = LINK[1]
			url = LINK[2]

			if len(LINK) > 3:
				icon = LINK[3]

			else:
				icon = ICON_GLOBE

			mitm = wx.MenuItem(parent.menu_policy, link_id, label, url)
			mitm.SetBitmap(icon)
			parent.menu_policy.AppendItem(mitm)

			wx.EVT_MENU(parent, link_id, parent.OpenPolicyManual)

	mitm_manual = wx.MenuItem(menu_help, wx.ID_HELP, GT(u'手册'), GT(u'打开使用手册'))
	mitm_about = wx.MenuItem(menu_help, wx.ID_ABOUT, GT(u'关于'), GT(u'关于Debreate'))

	menu_help.AppendMenu(-1, GT(u'链接'), parent.menu_policy)
	menu_help.AppendSeparator()
	menu_help.AppendItem(mitm_manual)
	menu_help.AppendItem(mitm_about)

	if parent.menu_opt.GetMenuItemCount():
		menubar.Append(parent.menu_opt, GT(u'选项'), menuid.OPTIONS)

	menubar.Append(menu_help, GT(u'帮助'), menuid.HELP)

	# catching menu events

	wx.EVT_MENU(parent, menuid.CCACHE, parent.OnClearCache)

	return menubar
