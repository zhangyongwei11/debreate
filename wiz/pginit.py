# -*- coding: utf-8 -*-

## \package wiz.pginit
#
#  The initial wizard page

# MIT licensing
# See: docs/LICENSE.txt


import wx

from dbr.language	import GT
from globals.ident	import pgid
from ui.hyperlink	import Hyperlink
from ui.layout		import BoxSizer
from wiz.wizard		import WizardPage


## The initial wizard page
class Page(WizardPage):
	def __init__(self, parent):
		WizardPage.__init__(self, parent, pgid.GREETING)

		m1 = GT(u'欢迎使用Debreate!')
		m2 = GT(u'Debreate是用来给安装在基于Debian的linux系统上的软件打包的工具。 点击右上角的箭头或者 点击页面菜单在整个程序中导航. 要获得其他关于debian软件包的信息，请使用help菜单项的子项导航链接.')
		m3 = GT(u'点击下方链接观看视频或者图文版教程.')
		str_info = u'{}\n\n{}\n\n{}'.format(m1, m2, m3)

		# --- Information to be displayed about each mode
		txt_info = wx.StaticText(self, label=str_info)
		# Keep characters within the width of the window
		txt_info.Wrap(600)

		lnk_video = Hyperlink(self, wx.ID_ANY, GT(u'使用Debreate构建软件包的视频教程'),
				u'https://www.bilibili.com/video/BV1xf4y1U7ZU?t=53')
		lnk_video.SetToolTipString(lnk_video.url)

		lnk_image = Hyperlink(self, wx.ID_ANY, GT(u'使用Debreate构建软件包的图文教程'),
				u'https://bbs.deepin.org/post/195472#mod=viewthread&tid=195472')
		lnk_image.SetToolTipString(lnk_image.url)

		# *** Layout *** #

		lyt_info = wx.GridSizer()
		lyt_info.Add(txt_info, 1, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL)

		lyt_main = BoxSizer(wx.VERTICAL)
		lyt_main.Add(lyt_info, 4, wx.ALIGN_CENTER|wx.ALL, 10)
		lyt_main.Add(lnk_video, 2, wx.ALIGN_CENTER)
		lyt_main.Add(lnk_image, 2, wx.ALIGN_CENTER)

		self.SetAutoLayout(True)
		self.SetSizer(lyt_main)
		self.Layout()
